#! /bin/sh
echo -e "enter the name of the file : \c"
read file_name

# -e flag is used to check wheather file exists or not
#if [ -e $file_name ]

#if [ -f $file_name ]
# -f flag is used to check file exists and regular file or not.

#if [ -d $file_name ]
# -d flag is used to check wheather directory exists or not.

if [ -s $file_name ]
# -s flag is used to check wheather file is empty or not...
#then
 #echo "$file_name not empty"
#else
# echo "$file_name empty"
# fi

then
 echo "$file_name found"
else
 echo "$file_name not found"
 fi