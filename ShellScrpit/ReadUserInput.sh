#! /bin/bash

echo "Enter name :"
# if we want to read any input from terminal,use read command
read name
echo Entered name is: $name

#if we want to read multiple users
echo "Enter names:"
read name1 name2 name3
echo Entered names are: $name1 , $name2 , $name3

