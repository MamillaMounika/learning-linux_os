#! /bin/bash

count=10
#if [ $count -eq 10 ]    # equal condition
#then 
#    echo "condition is true" 
#fi

#if [ $count -ne 1 ]    # not equal condition
#then 
 #  echo "condition is true" 
#fi 

#if [ $count -gt 12 ]    # greater than  condition
#then 
#  echo "condition is true" 
#fi 


#if [ $count -ge 15 ]    # greater than or equal to  condition
#then 
#  echo "condition is true" 
#fi 



#if [ $count -lt 19 ]    #Less than  condition
#then 
#  echo "condition is true" 
#fi 



if (( $count < 12 ))   # Less than  condition
then 
  echo "condition is true" 
fi 
