#! /bin/bash

#passing arguments to bash script
echo  $0 $1 $2 $3  ' > echo  $1 $2 $3'

# store arguments into an array
# $@ stores our agruments as an array, to pass them need to declare an array
#args=("$@")              
#echo ${args[0]}, ${args[1]} ${args[2]}

# if we want to print all at a time.
#echo $@
#if we want to know how many arguments passed, by writing #
#echo $#