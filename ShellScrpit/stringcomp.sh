#! /bin/bash


#possibiltes strings
#=, ==, !=, <, >, -z(string is null, that is, has zero length)

word=abc
#if [ $word == "bcd" ]       #string comparison
#then
#echo "Condition is true"
#fi

if [[ $word < "def" ]]     #less than comparison
then
echo "Condition is true"
else
echo "Condition is false"
fi

